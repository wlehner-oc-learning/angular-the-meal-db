import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ProseWrapperComponent } from './components/prose-wrapper/prose-wrapper.component';
import { NavbarComponent } from './components/navbar/navbar.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    standalone: true,
    imports: [NavbarComponent, ProseWrapperComponent, RouterOutlet]
})
export class AppComponent {
  title = 'Meal Data';
}
