import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { map } from 'rxjs';
import { RouterLink } from '@angular/router';
import { NgIf, NgFor } from '@angular/common';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    standalone: true,
    imports: [NgIf, NgFor, RouterLink]
})
export class IndexComponent {

  protected regions: string[] | undefined
  protected ingredients: string[] | undefined

  protected ingredientsExpanded = false

  protected get ingredientsToShow() {
    if(this.ingredientsExpanded) {
      return this.ingredients ?? [];
    }
    // Only show first 20 ingredients
    return (this.ingredients ?? []).filter ( (_, i) => i < 20)
  }

  private fetchRegions() {
    this.http.get<{meals: {strArea: string}[]}>("https://www.themealdb.com/api/json/v1/1/list.php?a=list")
      .pipe(
        map( e => (e.meals ?? []).map( e => e.strArea ).filter(Boolean))
      ).subscribe( e => {
        console.log(e)
        return this.regions = e;
      })
  }

  private fetchIngredients() {
    this.http.get<{meals: {strIngredient: string}[]}>("https://www.themealdb.com/api/json/v1/1/list.php?i=list")
    .pipe(
      map( e => (e.meals ?? []).map( e => e.strIngredient ).filter(Boolean))
    ).subscribe( e => {
      console.log(e)
      return this.ingredients = e;
    })
  }


  constructor(private http: HttpClient) {
    this.fetchRegions()
    this.fetchIngredients()
  }


}
