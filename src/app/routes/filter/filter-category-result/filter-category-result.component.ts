import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs';
import { FilteredDataService } from 'src/app/services/filtered-data.service';
import { CategoryDTO, convertFromDto as convertCategoryFromDto } from 'src/app/types/categoryDTO';
import { ShallowMeal,  } from 'src/app/types/shallowMealDTO';
import { FilterResultComponent } from '../../../components/filter-result/filter-result.component';
import { NgIf } from '@angular/common';

@Component({
    selector: 'app-filter-category-result',
    templateUrl: './filter-category-result.component.html',
    standalone: true,
    imports: [NgIf, FilterResultComponent]
})
export class FilterCategoryResultComponent {
  protected categoryName: string;

  protected meals: ShallowMeal[] | undefined
  protected error: string | undefined
  protected description: string | undefined

  constructor(private http: HttpClient, private dataService: FilteredDataService, activatedRoute: ActivatedRoute) {
    this.categoryName = activatedRoute.snapshot.paramMap.get("categoryName") ?? "None"
    this.dataService.getMealsByCategory$(this.categoryName).subscribe(
      e => this.meals = e
    )
    this.fetchDescription()
  }

  private fetchDescription() {
    this.http.get<{categories: CategoryDTO[]}>("https://www.themealdb.com/api/json/v1/1/categories.php")
    .pipe(
      map( e => e.categories ?? []),
      map( e => {
        const categoryElement = e.find( entry => entry.strCategory.toLowerCase().trim() === this.categoryName.toLowerCase().trim())
        if(!categoryElement) {
          return undefined
        }
        return convertCategoryFromDto(categoryElement)
      })
    ).subscribe( result => {
      if (!result) {
        // No Element with that category name found. Show error
        this.error = "No category found that is called "+ this.categoryName
        return
      }
      this.error = undefined
      this.description = result.description


    })
  }

}
