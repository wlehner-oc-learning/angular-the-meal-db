import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilteredDataService } from 'src/app/services/filtered-data.service';
import { ShallowMeal, ShallowMealDTO } from 'src/app/types/shallowMealDTO';
import { FilterResultComponent } from '../../../components/filter-result/filter-result.component';
import { NgIf } from '@angular/common';

@Component({
    selector: 'app-filter-region-result',
    templateUrl: './filter-region-result.component.html',
    standalone: true,
    imports: [NgIf, FilterResultComponent]
})
export class FilterRegionResultComponent {

  protected regionName: string;

  protected meals: ShallowMeal[] | undefined

  constructor(private dataService: FilteredDataService, activatedRoute: ActivatedRoute) {
    this.regionName = activatedRoute.snapshot.paramMap.get("regionName") ?? "No Region Provided"
    this.dataService.getMealsByArea$(this.regionName).subscribe(
      e => this.meals = e
    )
  }



}
