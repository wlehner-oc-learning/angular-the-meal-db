import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilteredDataService } from 'src/app/services/filtered-data.service';
import { ShallowMeal } from 'src/app/types/shallowMealDTO';
import { FilterResultComponent } from '../../../components/filter-result/filter-result.component';
import { NgIf } from '@angular/common';

@Component({
    selector: 'app-filter-ingredient-result',
    templateUrl: './filter-ingredient-result.component.html',
    standalone: true,
    imports: [NgIf, FilterResultComponent]
})
export class FilterIngredientResultComponent {
  protected ingredientName: string;

  protected meals: ShallowMeal[] | undefined

  constructor(private dataService: FilteredDataService, activatedRoute: ActivatedRoute) {
    this.ingredientName = activatedRoute.snapshot.paramMap.get("ingredientName") ?? "None"
    this.dataService.getMealsByIngredient$(this.ingredientName).subscribe(
      e => this.meals = e
    )
  }
}
