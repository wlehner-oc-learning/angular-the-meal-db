import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable, map } from 'rxjs';

import {Category, CategoryDTO, convertFromDto} from "../../types/categoryDTO"
import { CategoriesListEntryComponent } from '../../components/categories-list-entry/categories-list-entry.component';
import { NgIf, NgFor } from '@angular/common';



@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    standalone: true,
    imports: [NgIf, NgFor, CategoriesListEntryComponent]
})
export class CategoriesComponent implements OnInit {

  declare categories$: Observable<Category[]>;
  protected categories: Category[] | undefined = undefined

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    this.categories$ = this.httpClient
      .get<{categories: CategoryDTO[]}>("https://www.themealdb.com/api/json/v1/1/categories.php")
      .pipe(map( (dtoList) => {
        return dtoList.categories.map( dtoEntry => {
         return convertFromDto(dtoEntry)
        }).sort( (a,b) => {
          if(a.name < b.name) {
            return -1
          }
          if (a.name > b.name) {
            return 1;
          }
          return 0;
        })
      }))
    this.categories$.subscribe( (result) => {
      this.categories = result
    });
  }



}
