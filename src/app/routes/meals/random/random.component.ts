import { Component } from '@angular/core';
import { MealsIdComponent } from '../id/id.component';
import { MealDataComponent } from '../../../components/meal-data/meal-data.component';
import { NgIf } from '@angular/common';

@Component({
    selector: 'app-random-meal',
    templateUrl: '../id/id.component.html',
    standalone: true,
    imports: [NgIf, MealDataComponent]
})
export class RandomMealComponent extends MealsIdComponent {

  protected override getApiEndpoint(_: string | number): string {
    return "https://www.themealdb.com/api/json/v1/1/random.php"
  }

  protected override validateInput(id: number): boolean {
    // as  the ID is not used at all, we can just pass true here.
    // that way, validation does not fail. The getApiEndpoint
    // will return a URL that returns a random product.
    return true
  }
}
