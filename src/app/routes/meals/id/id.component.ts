import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs';
import { Meal, MealDTO, convertMealFromDto } from 'src/app/types/mealDTO';
import { MealDataComponent } from '../../../components/meal-data/meal-data.component';
import { NgIf } from '@angular/common';

type Status = "Loading" | "Finished" | "NotFound"


@Component({
    selector: 'app-meals-id',
    templateUrl: './id.component.html',
    standalone: true,
    imports: [NgIf, MealDataComponent]
})
export class MealsIdComponent {

  protected heading: string | undefined
  protected status: Status = "Loading"
  protected readonly id: number
  protected errorMessage: string | undefined
  public meal: Meal | undefined


  protected validateInput(id: number) {
    if(Number.isNaN(id)) {
      this.errorMessage = "Invalid URL. The Meal ID must be an Integer!"
      return false
    }
    return true
  }


  constructor(private http: HttpClient, activatedRoute: ActivatedRoute ){
    const id = Number.parseInt(activatedRoute.snapshot.paramMap.get("id")!)
    this.id = id
    if(!this.validateInput(id)) {
      return;
    }
    this.fetchMeal(id)
  }

  protected getApiEndpoint(id: number | string) {
    return "https://www.themealdb.com/api/json/v1/1/lookup.php?i="+id
  }

  protected fetchMeal(id: number) {
    this.http.get<{meals: MealDTO[]}>(this.getApiEndpoint(id))
      .pipe(
        map( e => {
          if(e.meals === null || e.meals.length === 0) {
            return undefined
          }
          return convertMealFromDto(e.meals[0])
        } )
      )
      .subscribe( e => {
        if(!e) {
          this.status = "NotFound"
          return
        }
        this.status = "Finished"
        return this.meal = e;
      })
  }



}
