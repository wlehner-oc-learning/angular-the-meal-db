import { Component, Input } from '@angular/core';
import { Category } from 'src/app/types/categoryDTO';
import { RouterLink } from '@angular/router';

@Component({
    selector: 'app-categories-list-entry',
    templateUrl: './categories-list-entry.component.html',
    styles: [
        `
    .max-lines-3 {
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    overflow: hidden;
    }
  `
    ],
    standalone: true,
    imports: [RouterLink]
})
export class CategoriesListEntryComponent {

  @Input() declare public category: Category


}
