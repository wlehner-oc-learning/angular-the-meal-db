import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'app-filter-wrapper-component',
    templateUrl: './filter-wrapper-component.component.html',
    standalone: true,
    imports: [RouterOutlet]
})
export class FilterWrapperComponentComponent {

}
