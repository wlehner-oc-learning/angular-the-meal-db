import { Component, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { RouterLink } from '@angular/router';

interface Link {
  href: string
  label: string
}


@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    standalone: true,
    imports: [RouterLink, NgFor]
})
export class NavbarComponent {

  @Input() declare public leftLinks: Link[]
  @Input() declare public rightLinks: Link[]

  protected hasRightLinks() {
    return (this.rightLinks ?? []).length > 0
  }

}
