import { Component, Input } from '@angular/core';
import { Meal } from 'src/app/types/mealDTO';
import { NgIf, NgFor } from '@angular/common';

@Component({
    selector: 'app-meal-data',
    templateUrl: './meal-data.component.html',
    styleUrls: ['./meal-data.component.scss'],
    standalone: true,
    imports: [NgIf, NgFor]
})
export class MealDataComponent {

  @Input() declare meal: Meal

}
