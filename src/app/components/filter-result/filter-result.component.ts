import { Component, Input } from '@angular/core';
import { ShallowMeal } from 'src/app/types/shallowMealDTO';
import { RouterLink } from '@angular/router';
import { NgIf, NgFor, NgClass } from '@angular/common';

@Component({
    selector: 'app-filter-result',
    templateUrl: './filter-result.component.html',
    standalone: true,
    imports: [NgIf, NgFor, NgClass, RouterLink]
})
export class FilterResultComponent {
  @Input() declare meals: ShallowMeal[]
  @Input() declare error: string | undefined
}
