import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './routes/index/index.component';

import { NotFoundComponent } from './routes/not-found/not-found.component';


import { FilterWrapperComponentComponent } from './components/filter-wrapper-component/filter-wrapper-component.component';
import { FilterCategoryResultComponent } from './routes/filter/filter-category-result/filter-category-result.component';
import { FilterIngredientResultComponent } from './routes/filter/filter-ingredient-result/filter-ingredient-result.component';
import { FilterRegionResultComponent } from './routes/filter/filter-region-result/filter-region-result.component';

const routes: Routes = [
  {
    path: "",
    component: IndexComponent
  },
  {
    path: "categories",
    loadComponent: () => import("./routes/categories/categories.component").then( e => e.CategoriesComponent)
  },
  {
    path: "categories/:categoryName",
    redirectTo: "filter/categories/:categoryName"
  },
  {
    /**
     * TODO: How to disable the index route here?
     * As in... how to get /filter to redirect to 404?
     */
    path: "filter",
    component: FilterWrapperComponentComponent,
    children: [
      {
        path: "categories/:categoryName",
        component: FilterCategoryResultComponent
      },
      {
        path: "ingredients/:ingredientName",
        component: FilterIngredientResultComponent
      },
      {
        path: "regions/:regionName",
        component: FilterRegionResultComponent
      }
    ]
  },
  {
    path: "about",
    loadComponent: () => import("./routes/about/about.component").then(e => e.AboutComponent)
  },
  {
    path: "meals/random",
    loadComponent: () => import("./routes/meals/random/random.component").then(e => e.RandomMealComponent)
  },
  {
    path: "meals/:id",
    loadComponent: () => import("./routes/meals/id/id.component").then(e => e.MealsIdComponent)
  },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
