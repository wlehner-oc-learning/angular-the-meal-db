export interface ShallowMealDTO {
    strMeal: string
    strMealThumb?: string
    idMeal: string
}

export interface ShallowMeal {
    name: string
    thumbnailUrl?: string
    id: number
}

export function convertFromDto(dto: ShallowMealDTO) : ShallowMeal {
    const id = Number.parseInt(dto.idMeal)
    if(Number.isNaN(id)) {
        throw new Error("DTO contains invalid ID: "+dto.idMeal)
    }

    return {
        name: dto.strMeal,
        thumbnailUrl: dto.strMealThumb,
        id
    }
}