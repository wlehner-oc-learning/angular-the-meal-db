export interface CategoryDTO {
  idCategory: string
  strCategory: string
  strCategoryThumb: string
  strCategoryDescription: string
}

export interface Category {
id: number,
name: string,
thumbnailUrl: string,
description: string
}

export function convertFromDto(dtoEntry: CategoryDTO) : Category {
  const returnValue: Category = {
    id: Number(dtoEntry.idCategory),
    name: dtoEntry.strCategory,
    description: dtoEntry.strCategoryDescription,
    thumbnailUrl: dtoEntry.strCategoryThumb
  }
  return returnValue
}