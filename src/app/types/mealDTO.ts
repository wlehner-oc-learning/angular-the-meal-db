export interface MealDTO {
  idMeal: string
  strMeal: string
  strCategory: string
  strArea?: string
  strInstructions?: string
  strMealThumb?: string
  strTags?: string
  strYoutube?: string

  strIngredient1?: string
  strIngredient2?: string
  strIngredient3?: string
  strIngredient4?: string
  strIngredient5?: string
  strIngredient6?: string
  strIngredient7?: string
  strIngredient8?: string
  strIngredient9?: string
  strIngredient10?: string
  strIngredient11?: string
  strIngredient12?: string
  strIngredient13?: string
  strIngredient14?: string
  strIngredient15?: string
  strIngredient16?: string
  strIngredient17?: string
  strIngredient18?: string
  strIngredient19?: string
  strIngredient20?: string

  strMeasure1?: string
  strMeasure2?: string
  strMeasure3?: string
  strMeasure4?: string
  strMeasure5?: string
  strMeasure6?: string
  strMeasure7?: string
  strMeasure8?: string
  strMeasure9?: string
  strMeasure10?: string
  strMeasure11?: string
  strMeasure12?: string
  strMeasure13?: string
  strMeasure14?: string
  strMeasure15?: string
  strMeasure16?: string
  strMeasure17?: string
  strMeasure18?: string
  strMeasure19?: string
  strMeasure20?: string

  strSource?: string
}

export interface Meal {
  id: number
  meal: string
  category: string
  area?: string
  ingredients?: {
    ingredient: string,
    measure: string
  }[],
  instructions: string[],
  source?: string,
  tags?: string[],
  imageUrl?: string
}


function isContentfulString(input: unknown) : boolean {
  if(typeof input !== "string") {
    return false
  }
  if(input.trim().length === 0) {
    return false;
  }
  return true;
}


function extractInstructions(mealDto: MealDTO): Meal["ingredients"] {
  const ingredients: Meal["ingredients"] = []

  // Convert from the rather weird API Response to an Array
  for(let i = 1; i <= 20; i++) {
    const ingredientKey = `strIngredient${i}` as keyof(MealDTO);
    const measureKey = `strMeasure${i}` as keyof(MealDTO);

    const ingredientValue = mealDto[ingredientKey];
    const measureValue = mealDto[measureKey];

    if(!ingredientValue || !measureValue) {
      // We assume that any further values are empty.
      // This is also checked by isContentfulString below,
      // but this way TypeScript explicitly knows that this value is
      // not undefined.
      break;
    }


    if(!isContentfulString(mealDto[ingredientKey]) || !isContentfulString(mealDto[measureKey])) {
      // We assume that any further values are empty.
      break
    }

    ingredients.push({
      ingredient: ingredientValue,
      measure: measureValue
    })
  }
  return ingredients
}


export function convertMealFromDto(mealDto: MealDTO) : Meal {
  let imageUrl = undefined;
  if (isContentfulString(mealDto.strMealThumb)) {
    imageUrl = mealDto.strMealThumb;
  }
  return {
    id: Number.parseInt(mealDto.idMeal),
    area: mealDto.strArea,
    category: mealDto.strCategory,
    ingredients: extractInstructions(mealDto),
    instructions: (mealDto.strInstructions ?? "").split("\r\n").map( e => e.trim()).filter( e => e.length > 0),
    meal: mealDto.strMeal,
    source: mealDto.strSource,
    tags: (mealDto.strCategory ?? "").split(",").map(e => e.trim()),
    imageUrl
  }
}
