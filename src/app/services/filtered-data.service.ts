import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { ShallowMeal, ShallowMealDTO, convertFromDto as convertMealFromDto } from '../types/shallowMealDTO';

@Injectable({
  providedIn: 'root'
})
export class FilteredDataService {

  constructor(private http: HttpClient) {}

  private getObservableForCategory$(categoryName: string) {
    return this.http.get<{meals: ShallowMealDTO[] | null}>(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${categoryName}`)
  }

  private getObservableForArea$(area: string) {
    return this.http.get<{meals: ShallowMealDTO[] | null}>(`https://www.themealdb.com/api/json/v1/1/filter.php?a=${area}`)
  }

  private getObservableForIngredient$(ingredient: string) {
    return this.http.get<{meals: ShallowMealDTO[] | null}>(`https://www.themealdb.com/api/json/v1/1/filter.php?i=${ingredient}`)
  }

  private convertFromDtoToShallowMeal$(observable$: Observable<{meals: ShallowMealDTO[] | null}>) {
    return observable$.pipe(
      map( e => {
        const meals = e.meals ?? []
        const convertedMeals = meals.map ( entry => {
          try {
            return convertMealFromDto(entry)
          }
          catch( _ ) {
            return undefined
          }
        }).filter(Boolean) as ShallowMeal[] // TS does not understand that passing the Boolean Ctor as filter eliminated `undefined` values.
        return convertedMeals
      })
    )
  }



  public getMealsByCategory$(categoryName: string) {
    return this.convertFromDtoToShallowMeal$(this.getObservableForCategory$(categoryName))
  }

  public getMealsByArea$(areaName: string) {
    return this.convertFromDtoToShallowMeal$(this.getObservableForArea$(areaName))
  }

  public getMealsByIngredient$(ingredientName: string) {
    return this.convertFromDtoToShallowMeal$(this.getObservableForIngredient$(ingredientName))
  }





}
